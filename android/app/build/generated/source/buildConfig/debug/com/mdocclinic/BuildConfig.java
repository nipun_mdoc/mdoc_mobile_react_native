/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mdocclinic;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mdocclinic.dev";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String ANDROID_VERSION_CODE = "1";
  // Field from default config.
  public static final String ANDROID_VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String API_URL = "https://rollout9.mdoc.dev/iam-service";
  // Field from default config.
  public static final String APP_ID = "com.mdocclinic.dev";
  // Field from default config.
  public static final String APP_NAME = "mDoc Dev";
  // Field from default config.
  public static final String IOS_BUNDLE_ID = "com.mdocclinic.dev";
  // Field from default config.
  public static final String IOS_VERSION_CODE = "1";
  // Field from default config.
  public static final String IOS_VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String keyAlias = "dev";
  // Field from default config.
  public static final String keyPassword = "devapk";
  // Field from default config.
  public static final String storeFile = "dev2.jks";
  // Field from default config.
  public static final String storePassword = "devapk";
}
