/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mdocclinic;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mdocclinic";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "dev";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String ANDROID_VERSION_CODE = "1";
  // Field from default config.
  public static final String ANDROID_VERSION_CODE_DEV = "1";
  // Field from default config.
  public static final String ANDROID_VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String ANDROID_VERSION_NAME_DEV = "1.0.1";
  // Field from default config.
  public static final String APP_ID = "com.mdocclinic";
  // Field from default config.
  public static final String APP_ID_DEV = "com.mdocclinic.dev";
  // Field from product flavor: dev
  public static final String FLAV = "dev";
  // Field from default config.
  public static final String IOS_BUNDLE_ID = "com.mdocclinic";
  // Field from default config.
  public static final String IOS_BUNDLE_ID_DEV = "com.mdocclinic.dev";
  // Field from default config.
  public static final String IOS_VERSION_CODE = "1";
  // Field from default config.
  public static final String IOS_VERSION_CODE_DEV = "1";
  // Field from default config.
  public static final String IOS_VERSION_NAME = "1.0.1";
  // Field from default config.
  public static final String IOS_VERSION_NAME_DEV = "1.0.1";
}
