import React, {useEffect} from 'react';
import {View} from 'react-native';
import QuestionnaireComponent from "./QuestionnaireComponent";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { useNavigation } from "@react-navigation/core";


const QuestionnaireContainer = props => {
  const navigation = useNavigation()
  //const store = useStore();
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <QuestionnaireComponent {...props}/>
    </View>
  );
};

export default QuestionnaireContainer;
