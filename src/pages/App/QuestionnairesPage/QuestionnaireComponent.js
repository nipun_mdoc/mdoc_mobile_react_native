import React from "react";
import { Text, View } from "react-native";
import { AppTheme } from "../../../utils/app_style/AppTheme";

const QuestionnaireComponent = props => {
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <Text style={{fontSize: 40}}>Questionnaire</Text>
    </View>
  );
};

export default QuestionnaireComponent;
