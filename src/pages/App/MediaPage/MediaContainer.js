import React, {useEffect} from 'react';
import {View} from 'react-native';
import MediaComponent from "./MediaComponent";
import { AppTheme } from "../../../utils/app_style/AppTheme";


const MediaContainer = props => {
  // const navigation = useNavigation()
  //const store = useStore();
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <MediaComponent {...props}/>
    </View>
  );
};

export default MediaContainer;
