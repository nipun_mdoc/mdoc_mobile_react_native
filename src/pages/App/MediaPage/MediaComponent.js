import React from "react";
import { Text, View } from "react-native";
import { AppTheme } from "../../../utils/app_style/AppTheme";

const MediaComponent = props => {
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <Text style={{fontSize: 40}}>Media</Text>
    </View>
  );
};

export default MediaComponent;
