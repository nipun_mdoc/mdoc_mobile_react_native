import React, { useEffect, useState } from "react";
import {View} from 'react-native';
import MoreComponent from "./MoreComponent";
import AppConfig from "../../../core/app_config/AppConfig";
import { useNavigation } from "@react-navigation/core";
import { Features } from "../../../utils/Constant";
import { QuestionnairePage } from "../../index";
import { QUESTIONNAIRE_PAGE } from "../../../routes/RouteConst";

const MoreContainer = props => {
  const navigation = useNavigation()
  //const store = useStore();
  const [availableFeatures, setAvailableFeatures] = useState([])

  const openQuestionnaire = item => {
    navigation.navigate(QUESTIONNAIRE_PAGE)
  };

  useEffect(() => {
    let features = (new AppConfig()).getFeaturesList().AppConfig
    let list = features.filter((item) => item.IsEnabled === true && item.Id !== Features.MODULE_MORE)
    setAvailableFeatures(list)
  }, []);
  return (
    <View style={{flex: 1}}>
      <MoreComponent
        availableFeatures={availableFeatures}
        onAction={(item)=> {
          openQuestionnaire(item)
          // alert(item.FeatureTitle)
        }}
        {...props} />
    </View>
  );
};

export default MoreContainer;
