import React from "react";
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import MoreItem from "../../../utils/Component/MoreItem";
import { keyExtractor } from "../../../utils/Common";

const MoreComponent = props => {
  let {availableFeatures, onAction} = props
  return (
    <View style={{flex: 1}}>
      <View style={styles.mainViewStyle}>
        <View style={{flex: 1}}>
          <FlatList
            data={availableFeatures}
            renderItem={({item}) => <MoreItem item={item} onAction={(item) => {onAction(item)}}/>}
            keyExtractor={keyExtractor}
          />
        </View>
        <View style={styles.logoutView}>
          <TouchableOpacity>
            <Text style={styles.logoutTitle}>Log out</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  logoutTitle: {
    fontSize: AppTheme.FONT_SIZE.LARGE_FONT_SIZE,
    color: AppTheme.COLOR.BLACK_COLOR,
    fontFamily: AppTheme.FONT_FAMILY.FONT_MEDIUM
  },
  logoutView: {
    padding: 20,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  mainViewStyle: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: AppTheme.COLOR.BORDER_COLOR,
    flex: 1
  }
})

export default MoreComponent;
