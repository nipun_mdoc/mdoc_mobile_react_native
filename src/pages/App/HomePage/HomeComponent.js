import React from "react";
import { ScrollView, Text, View } from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import DashLogoView from "../../../utils/Component/DashLogoView";

const HomeComponent = props => {
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <LinearGradient colors={['rgba(251, 251, 251, 0.33)', 'rgba(169, 169, 169, 0.0)']}  >
         <DashLogoView/>

        </LinearGradient>
      </ScrollView>
    </View>
  );
};

export default HomeComponent;
