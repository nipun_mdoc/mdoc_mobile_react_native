import React, { useEffect } from "react";
import { View } from "react-native";
import HomeComponent from "./HomeComponent";
import { AppTheme } from "../../../utils/app_style/AppTheme";


const HomeContainer = props => {
  // const navigation = useNavigation()
  //const store = useStore();
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1}}>
      <HomeComponent {...props}/>
    </View>
  );
};

export default HomeContainer;
