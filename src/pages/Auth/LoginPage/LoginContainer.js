import React, { useEffect, useState } from "react";
import { View } from "react-native";
import LoginComponent from "./LoginComponent";
import { validateUser } from "../../../redux/actions/authActions";
import connect from "react-redux/lib/connect/connect";
import DeviceDetail from "../../../utils/DeviceInfoHelper";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { Constants } from "../../../utils/Constant";
import { useNavigation } from "@react-navigation/core";
import { FORGOT_PASSWORD_PAGE, LOGIN_TAN_PAGE, REGISTER_PAGE, HOME_PAGE } from "../../../routes/RouteConst";

const LoginContainer = props => {
  const navigation = useNavigation()
  const {loginResult, isLogout} = props;
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const doLogin = values => {
    DeviceDetail.getUserDeviceWithUniqueId().then((deviceStr) => {
      props.validateUser(deviceStr, username, password);
    });
  };
  const forgotPassword = values => {
    navigation.navigate(FORGOT_PASSWORD_PAGE)
  };
  const registerUser = values => {
    navigation.navigate(REGISTER_PAGE)
  };
  const loginWithTan = values => {
    navigation.navigate(LOGIN_TAN_PAGE)
  };
  const onChangeText = (text, type) => {
    if (type === Constants.LoginFieldType.Username) {
      setUsername(text);
    } else if (type === Constants.LoginFieldType.Password) {
      setPassword(text);
    }
  };
  useEffect(() => {
    console.log("aaaaaaaaaaa useEffect"+JSON.stringify(props.loginResult))
    if(props.loginResult){
      console.log("aaaaaaaaaaa useEffect home")
      navigation.navigate(HOME_PAGE)
    }
    // 
  }, [props]);
  return (
    <View style={{ flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR }}>
      <LoginComponent
        doLogin={doLogin}
        registerUser={registerUser}
        forgotPassword={forgotPassword}
        loginWithTan={loginWithTan}
        username={username}
        password={password}
        onChangeText={onChangeText}
        {...props} />
    </View>
  );
};


const mapStateToProps = (state) => {
  return {
    loginResult: state.authReducer,
  };
};

const mapDispatchToProps = {
  validateUser,
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
