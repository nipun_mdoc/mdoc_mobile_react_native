import React from "react";
import { ScrollView, View } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import commonStyles from "../../../utils/app_style/GlobalStyles";
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen";
import LoginForm from "./LoginForm";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { STANDARD_SCREEN_HEIGHT } from "../../../utils/Constant";
import LoginBottomView from "../../../utils/Component/LoginBottomView";
import LoginThemeImage from "../../../utils/Component/LoginThemeImage";

const LoginComponent = props => {
  return (
    <View style={{flex: 1, paddingBottom: 40}}>
      <ScrollView>
        <View style={{ flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR, paddingBottom: 40 }}>
          <LoginThemeImage />
          <View
            style={[
              commonStyles.shadowEffect,
              commonStyles.card,
              {
                width:
                  widthPercentageToDP("100%") -
                  RFValue(20 * 2, STANDARD_SCREEN_HEIGHT),
                height:
                  heightPercentageToDP("90%") -
                  RFValue(170, STANDARD_SCREEN_HEIGHT),
                marginHorizontal: RFValue(40, STANDARD_SCREEN_HEIGHT),
              },
            ]}>
            <LoginForm {...props} />
          </View>
        </View>
      </ScrollView>
      <LoginBottomView />
    </View>

  );
};

export default LoginComponent;
