import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import TextButton from "../../../utils/Component/TextButton";
import AppButtonBG from "../../../utils/Component/AppButtonWithBG";
import AppButton from "../../../utils/Component/AppButton";
import AppTextInput from "../../../utils/Component/AppTextInput";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { Constants, FieldIconType, STANDARD_SCREEN_HEIGHT } from "../../../utils/Constant";
import LoginTopTitleView from "../../../utils/Component/LoginTopTitleView";

const LoginForm = props => {
  const { username, password, onChangeText, doLogin, forgotPassword, registerUser, loginWithTan } = props;

  return (
    <View style={{ flex: 1 }}>
      <LoginTopTitleView
        title={'Log In'}
      />
      <View
        style={{
          marginHorizontal: RFValue(20, STANDARD_SCREEN_HEIGHT),
          marginTop: RFValue(8, STANDARD_SCREEN_HEIGHT),
        }}>
        <AppTextInput
          type={FieldIconType.USER}
          label="Username"
          value={username}
          setFieldValue={(text) => {
            onChangeText(text, Constants.LoginFieldType.Username);
          }}
        />
        <View style={{ height: 15 }} />
        <AppTextInput
          type={FieldIconType.PASSWORD}
          label="Password"
          value={password}
          secureTextEntry={true}
          setFieldValue={(text) => {
            onChangeText(text, Constants.LoginFieldType.Password);
          }}
        />
        <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
          <TextButton
            onAction={() => {
              forgotPassword()
            }}
            title={"Forgot Password?"}
            {...props} />
          <TextButton
            onAction={() => {
              registerUser()
            }}
            title={"Registration"}
            {...props} />
        </View>
        <View style={{ height: 30 }} />
        <AppButtonBG
          onAction={() => {
            doLogin();
          }}
          title={"Sign In"}
          {...props} />
        <View style={{ height: 15 }} />
        <AppButton
          onAction={() => {

          }}
          title={"Scan QR code"}
          {...props} />
        <View style={{ height: 15 }} />
        <AppButton
          onAction={() => {
            loginWithTan()
          }}
          title={"Login With TAN"}
          {...props} />
        <View style={{ height: 25 }} />
        <View style={{ justifyContent: "space-between", flexDirection: "row", height: 40 }}>
          <View style={{ justifyContent: "flex-start", paddingTop: 7 }}>
            <Image
              source={require("../../../assets/images/common/login/m.Doc.png")}
              style={{ height: 33, width: 77, resizeMode: "contain" }}
            />
          </View>
          <View>
            <Image
              source={require("../../../assets/images/common/login/reddot.png")}
              style={{ height: 40, width: 74, resizeMode: "contain" }}
            />
          </View>
        </View>
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  imageBGView: {
    width: "100%",
    height: RFValue(120, STANDARD_SCREEN_HEIGHT),
    justifyContent: "center",
    alignItems: "center",
  },
  topImage: {
    width: RFValue(160, STANDARD_SCREEN_HEIGHT),
    height: RFValue(80, STANDARD_SCREEN_HEIGHT),
    resizeMode: "contain",
  },
  loginLabel: {
    fontSize: RFValue(AppTheme.FONT_SIZE.LARGE_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
    fontFamily: AppTheme.FONT_FAMILY.FONT_MEDIUM,
    justifyContent: "center",
    marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT),
  },
});

export default LoginForm;
