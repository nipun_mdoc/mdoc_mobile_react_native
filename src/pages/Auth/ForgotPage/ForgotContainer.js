import React, { useEffect } from "react";
import { View } from "react-native";
import ForgotComponent from "./ForgotComponent";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { useNavigation } from "@react-navigation/core";

const ForgotContainer = props => {
  const navigation = useNavigation()
  //const store = useStore();
  const goBackLogin = values => {
    navigation.goBack()
  };
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <ForgotComponent
        goBackLogin={goBackLogin}
        {...props}
      />
    </View>
  );
};

export default ForgotContainer;
