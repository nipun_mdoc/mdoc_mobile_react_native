import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import commonStyles from "../../../utils/app_style/GlobalStyles";
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { FieldIconType, STANDARD_SCREEN_HEIGHT } from "../../../utils/Constant";
import LoginBottomView from "../../../utils/Component/LoginBottomView";
import LoginTopTitleView from "../../../utils/Component/LoginTopTitleView";
import LoginThemeImage from "../../../utils/Component/LoginThemeImage";
import AppTextInput from "../../../utils/Component/AppTextInput";
import InfoIcon from "../../../assets/images/common/login/info.svg";
import AppButtonBG from "../../../utils/Component/AppButtonWithBG";
import GoBackWithTitle from "../../../utils/Component/GoBackWithTitle";

const ForgotComponent = props => {
  let { goBackLogin } = props;

  return (
    <View style={{ flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR, paddingBottom: 40  }}>
      <View style={{flex: 1}}>
        <LoginThemeImage/>
        <View
          style={[
            commonStyles.shadowEffect,
            commonStyles.card,
            {
              width:
                widthPercentageToDP("100%") -
                RFValue(20 * 2, STANDARD_SCREEN_HEIGHT),
              height:
                heightPercentageToDP("65%") -
                RFValue(140, STANDARD_SCREEN_HEIGHT),
              marginHorizontal: RFValue(40, STANDARD_SCREEN_HEIGHT),
            },
          ]}>
          <View style={{ flex: 1 }}>
            <LoginTopTitleView
              title={'Forgot Password'}
            />
            <View
              style={{
                marginHorizontal: RFValue(20, STANDARD_SCREEN_HEIGHT),
                marginTop: RFValue(8, STANDARD_SCREEN_HEIGHT),
              }}>
              <AppTextInput
                type={FieldIconType.USER}
                label="Username"
                value={''}
                setFieldValue={(text) => {
                  // onChangeText(text, Constants.LoginFieldType.Username);
                }}
              />
              <View style={{height: 15}}/>
              <View style={{flexDirection: 'row'}}>
                <View style={{height: 16, width: 21}}>
                  <InfoIcon/>
                </View>
                <Text style={styles.labelStyle}>Geben Sie Ihren Benutzernamen oder Ihre E-Mail-Adresse ein und wir senden Ihnen Anweisungen zum Erstellen eines neuen Passworts.</Text>
              </View>
              <View style={{height: 15}}/>
              <AppButtonBG
                onAction={() => {
                }}
                title={"Forgot Password"}
                {...props} />
              <View style={{height: 15}}/>
              <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                <GoBackWithTitle
                  title={'Back to Login'}
                  onAction={() => {
                    goBackLogin();
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
      <LoginBottomView />
    </View>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    fontSize: AppTheme.FONT_SIZE.SMALL_FONT_SIZE,
    color: AppTheme.TEXT_COLOR.GRAY_COLOR
  }
});
export default ForgotComponent;
