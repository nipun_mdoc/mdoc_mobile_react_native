import React, { useEffect } from "react";
import { View } from "react-native";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { useNavigation } from "@react-navigation/core";
import LoginTanComponent from "./LoginTanComponent";

const LoginTanContainer = props => {
  const navigation = useNavigation()
  //const store = useStore();
  const goBackLogin = values => {
    navigation.goBack()
  };
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <LoginTanComponent
        goBackLogin={goBackLogin}
        {...props}
      />
    </View>
  );
};

export default LoginTanContainer;
