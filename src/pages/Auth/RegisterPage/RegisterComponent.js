import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import commonStyles from "../../../utils/app_style/GlobalStyles";
import { heightPercentageToDP, widthPercentageToDP } from "react-native-responsive-screen";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { FieldIconType, STANDARD_SCREEN_HEIGHT } from "../../../utils/Constant";
import LoginBottomView from "../../../utils/Component/LoginBottomView";
import LoginTopTitleView from "../../../utils/Component/LoginTopTitleView";
import AppTextInput from "../../../utils/Component/AppTextInput";
import GoBackWithTitle from "../../../utils/Component/GoBackWithTitle";
import AppButtonBG from "../../../utils/Component/AppButtonWithBG";
import AppButton from "../../../utils/Component/AppButton";

const RegisterComponent = props => {
  let { goBackLogin } = props;

  return (
    <View style={{ flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR, paddingBottom: 40 }}>
     <ScrollView>
       <View
         style={[
           commonStyles.shadowEffect,
           commonStyles.card,
           {
             width:
               widthPercentageToDP("100%") -
               RFValue(20 * 2, STANDARD_SCREEN_HEIGHT),
             height:
               heightPercentageToDP("130%"),
             marginHorizontal: RFValue(40, STANDARD_SCREEN_HEIGHT),
             marginTop: 60,
           },
         ]}>
         <View style={{ flex: 1 }}>
           <LoginTopTitleView
             title={"Registration"}
           />
           <View
             style={styles.mainView}>
             <AppTextInput
               type={FieldIconType.USER}
               label="Username"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.NAME}
               label="First Name"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.NAME}
               label="Last Name"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.EMAIL}
               label="Email"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.PHONE}
               label="Telephone(Mobile)"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.PASSWORD}
               label="Password"
               value={""}
               secureTextEntry={true}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.PASSWORD}
               label="Confirm Password"
               value={""}
               secureTextEntry={true}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.USER}
               label="Username"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <View style={{flexDirection: 'row', justifyContent: 'center'}}>
               <Text style={styles.loginLabel}>Emergency Contact</Text>
             </View>
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.NAME}
               label="First Name"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.NAME}
               label="Last Name"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 15 }} />
             <AppTextInput
               type={FieldIconType.PHONE}
               label="Telephone(Mobile)"
               value={""}
               setFieldValue={(text) => {
                 // onChangeText(text, Constants.LoginFieldType.Username);
               }}
             />
             <View style={{ height: 30 }} />
             <AppButtonBG
               onAction={() => {
                 doLogin();
               }}
               title={"Register"}
               {...props} />
             <View style={{ height: 15 }} />
             <View style={{ justifyContent: "center", flexDirection: "row" }}>
               <GoBackWithTitle
                 title={"Back to Login"}
                 onAction={() => {
                   goBackLogin();
                 }}
               />
             </View>
           </View>
         </View>
       </View>
     </ScrollView>
      <LoginBottomView />
    </View>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    fontSize: AppTheme.FONT_SIZE.SMALL_FONT_SIZE,
    color: AppTheme.TEXT_COLOR.GRAY_COLOR,
  },
  mainView: {
    marginHorizontal: RFValue(20, STANDARD_SCREEN_HEIGHT),
    marginTop: RFValue(8, STANDARD_SCREEN_HEIGHT),
  },
  loginLabel: {
    fontSize: RFValue(AppTheme.FONT_SIZE.LARGE_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
    fontFamily: AppTheme.FONT_FAMILY.FONT_MEDIUM,
    justifyContent: "center",
    marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT),
  },
});
export default RegisterComponent;
