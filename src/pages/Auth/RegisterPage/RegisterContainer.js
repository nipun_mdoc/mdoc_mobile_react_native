import React, { useEffect } from "react";
import { View } from "react-native";
import { AppTheme } from "../../../utils/app_style/AppTheme";
import { useNavigation } from "@react-navigation/core";
import RegisterComponent from "./RegisterComponent";

const RegisterContainer = props => {
  const navigation = useNavigation()
  //const store = useStore();
  const goBackLogin = values => {
    navigation.goBack()
  };
  useEffect(() => {}, []);
  return (
    <View style={{flex: 1, backgroundColor: AppTheme.COLOR.THEME_COLOR}}>
      <RegisterComponent
        goBackLogin={goBackLogin}
        {...props}
      />
    </View>
  );
};

export default RegisterContainer;
