import _SplashPage from "./SplashPage/SplashContainer";
import _LoginPage from "./../pages/Auth/LoginPage/LoginContainer";
import _ForgotPassword from "./../pages/Auth/ForgotPage/ForgotContainer";
import _HomePage from "./../pages/App/HomePage/HomeContainer";
import _MediaPage from "./../pages/App/MediaPage/MediaContainer";
import _MorePage from "./../pages/App/MorePage/MoreContainer";
import _QuestionnairePage from "./../pages/App/QuestionnairesPage/QuestionnaireContainer";
import _RegisterPage from "./../pages/Auth/RegisterPage/RegisterContainer";
import _LoginWithTan from "./../pages/Auth/LoginTanPage/LoginTanContainer";

export const SplashPage = _SplashPage;
export const LoginPage = _LoginPage;
export const ForgotPage = _ForgotPassword;
export const HomePage = _HomePage;
export const MediaPage = _MediaPage;
export const MorePage = _MorePage;
export const QuestionnairePage = _QuestionnairePage;
export const RegisterPage = _RegisterPage;
export const LoginWithTan = _LoginWithTan;


