import AppConfig from "../../../core/app_config/AppConfig";
import { DefaultHeaders, HTTP_METHOD, HTTP_STATUS } from "./APIConstant";
import { mapFailureResponseJSONToModel, mapSuccessResponseJSONToModel } from "../../../core/mapper/ResponseMapper";
import LOG from "../../log/Log";

class APIClient {

  /**
   * API Client
   * */
  constructor(props) {
    this.baseUrl = (new AppConfig()).getAPIBaseUrl();
  }


  /**
   * Create Complete URL
   * @param: urlPath
   * @complete_url: baseUrl + urlPath
   * */
  _getCompleteUrl(path, queryParams) {
    let url = this.baseUrl + path;
    if (queryParams && Object.keys(queryParams).length > 0) {
      let queryStr = this._getQueryString(queryParams);
      url = url + '?' + queryStr;
    }
    return url;
  }

  _getQueryString(params) {
    let esc = encodeURIComponent;
    return Object.keys(params)
      .map(k => esc(k) + '=' + esc(params[k]))
      .join('&');
  }


  /**
   * HTTP Headers
   * */
  _getHTTPHeaders(extraHeaders) {
    let headers = {};
    Object.assign(headers, DefaultHeaders);
    Object.assign(headers, extraHeaders);
    //Auth Token

    return headers
  }


  /**
   * Send HTTP Request
   * @param: config:->> path, method, queryParams, postParams, extraHeaders.
   * */
  async httpRequest(config) {
    let { path, method, queryParams, postParams, extraHeaders } = config;

    console.log('API REQUEST:: ', config);

    //Complete Url
    let url = this._getCompleteUrl(path, queryParams);

    //Http Headers
    // let token = await AuthStorage.retrieve();
    let headers = this._getHTTPHeaders(extraHeaders);
    // if (token !== null) {
    //   headers.token = token;
    // }
    //Send HTTP Request

    let response = await fetch(
      url,
      {
        method: method,
        headers: headers,
        body: (method === HTTP_METHOD.GET) ? '' : JSON.stringify(postParams),
      }
    );

    //Check response Status
    if (response.status === HTTP_STATUS.OK) {
      //Success Result
      let result = await response.json();
      LOG.log("API Request & Response::::  ", url);
      LOG.log("Headers:: ", headers);
      LOG.log("Method:: ", method);
      LOG.log("PostParam:: ",postParams);
      LOG.log("Response :: ", result);

      LOG.log("Result::::", JSON.stringify(result))

      if (result.code === "200"){
        return mapSuccessResponseJSONToModel(result);
      }else {
        return mapFailureResponseJSONToModel(result);
      }
    }

    let {
      status, statusText
    } = response;
    LOG.log("API Request & Response::  ", url);
    LOG.log("Headers:: ", headers);
    LOG.log("Method:: ", method);
    LOG.log("Response:: ", response);

    let message = (statusText && statusText.length > 0) ? statusText : '';
    if (message.length === 0) {
        message = "Network Problem";
    }

    return mapFailureResponseJSONToModel({
      code: '201',
      message: message
    });
  }
}


export default APIClient;
