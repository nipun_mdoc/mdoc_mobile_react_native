const DefaultHeaders = {
  'Content-Type': 'application/json',
};


const HTTP_STATUS = {
  OK: 200,
  UNAUTHORIZED: 401
};


const HTTP_METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'

};


const HTTP_ERROR_CODE = {
  ERROR_EXCEPTION: -1

};

export {
  DefaultHeaders,
  HTTP_STATUS,
  HTTP_METHOD,
  HTTP_ERROR_CODE
}
