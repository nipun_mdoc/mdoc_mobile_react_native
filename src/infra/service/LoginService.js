import APIClient from "./api_clients/APIClient";
import { ApiUrlPath } from "./api_clients/APIURLPath";
import { HTTP_METHOD } from "./api_clients/APIConstant";

class LoginService {

  constructor(props) {
    this.client = new APIClient();
  }

  async loginUser(param) {
    let serviceUrl = ApiUrlPath.AUTH.LOGIN;
    let result = await this.client.httpRequest({
      path: serviceUrl,
      method: HTTP_METHOD.POST,
      postParams: param,
    });
    let {code, message, data} = result;
    return {code, message, data};
  }

}


export default LoginService;
