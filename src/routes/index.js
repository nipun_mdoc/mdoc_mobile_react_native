import "react-native-gesture-handler";
import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import * as RouteConst from "./RouteConst";
import {
  ForgotPage,
  HomePage,
  LoginPage,
  MediaPage,
  MorePage,
  QuestionnairePage,
  SplashPage,
  RegisterPage,
  LoginWithTan,
} from "../pages/";

import { Animated, Dimensions, StyleSheet, View } from "react-native";
import { connect, useStore } from "react-redux";
import { RFValue } from "react-native-responsive-fontsize";
import BottomCenterView from "../utils/Component/components/BottomCenterView/BottomCenterView";
import commonStyles from "../utils/app_style/GlobalStyles";
import { AppTheme } from "../utils/app_style/AppTheme";
import { STANDARD_SCREEN_HEIGHT } from "../utils/Constant";
import * as TAB_ICONS from "../assets/images/tabs";

const MainStack = createStackNavigator();
const MainTab = createBottomTabNavigator();

const studentTabBarIcons = [TAB_ICONS.HOME, TAB_ICONS.FD_CASH, TAB_ICONS.PROFILE, TAB_ICONS.SETTINGS];


function CustomTabBar({ state, descriptors, navigation, loginData }) {
  const [translateValue] = React.useState(new Animated.Value(0));
  const totalWidth = Dimensions.get("window").width;
  const tabWidth = totalWidth / state.routes.length;

  return (<View style={[style.tabContainer, { width: totalWidth }]}>
      <View style={{ flexDirection: "row" }}>
        <Animated.View
          style={[
            style.slider,
            {
              transform: [{ translateX: translateValue }],
              //width: tabWidth - 20,
            },
          ]}
        />
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;
          const iconName = studentTabBarIcons[index];
          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }

            Animated.spring(translateValue, {
              toValue: index * tabWidth,
              velocity: 10,
              useNativeDriver: true,
            }).start();
          };

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            });
          };

          return (<BottomCenterView
              color={AppTheme.COLOR.WHITE_COLOR}
              iconImageSize={24}
              title={label}
              imageName={iconName}
              onPress={onPress}
              onLongPress={onLongPress}
              index={index + 1}
              numberOfTabs={state.routes.length}
            >
            </BottomCenterView>
          );
        })}
      </View>
    </View>
  );
}

function MoreStack() {
  return (
    <MainStack.Navigator>
      <MainStack.Screen name="More" component={MorePage} />
      <MainStack.Screen name={RouteConst.QUESTIONNAIRE_PAGE} component={QuestionnairePage} />
    </MainStack.Navigator>
  )
}


export const MainRoute = (props) => {
  // const navigation = useNavigation();
  //const [loginData, setLoginData] = React.useState(undefined);
  const { loginResult, isLogout } = props;
  const store = useStore();
  const [loading, setLoading] = React.useState(true);

  const getLoginData = async () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 5000);
  };

  React.useEffect(() => {
    getLoginData();
  }, []);

  if (loading) {
    return <View style={{ flex: 1 }}>
      <SplashPage />
    </View>;
  }

  console.log('aaaaaaaaaaaaaa nabigation loginResult '+loginResult)
  if (loginResult) {
    return <MainTab.Navigator
      tabBar={props => <CustomTabBar loginData={loginResult} {...props} />}
      tabBarOptions={{
        activeTintColor: AppTheme.COLOR.WHITE_COLOR,
        inactiveTintColor: AppTheme.COLOR.WHITE_COLOR,
        labelStyle: commonStyles.bottomTabText,
        style: commonStyles.bottomContainer,
        tabStyle: commonStyles.tabStyle,
      }}
      initialRouteName={"Tab1"}>
      <MainTab.Screen
        name={"Tab1"}
        component={HomePage}
      />
      <MainTab.Screen
        name={"Tab2"}
        component={MediaPage}
      />
      <MainTab.Screen
        name={"Tab3"}
        component={QuestionnairePage}
      />
      <MainTab.Screen
        name={"Tab4"}
        component={MoreStack}
      />
    </MainTab.Navigator>;
  }

  return (<MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <MainStack.Screen
        name={RouteConst.LOGIN_PAGE}
        component={LoginPage}
      />
      <MainStack.Screen
        name={RouteConst.FORGOT_PASSWORD_PAGE}
        component={ForgotPage}
      />
      <MainStack.Screen
        name={RouteConst.REGISTER_PAGE}
        component={RegisterPage}
      />
      <MainStack.Screen
        name={RouteConst.LOGIN_TAN_PAGE}
        component={LoginWithTan}
      />
    </MainStack.Navigator>
  );

};

const style = StyleSheet.create({
  tabContainer: {
    height: RFValue(60, STANDARD_SCREEN_HEIGHT),
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.0,
    backgroundColor: "white",
    //borderTopRightRadius: 20,
    //borderTopLeftRadius: 20,
    elevation: 10,
    position: "absolute",
    bottom: 0,
  },
  slider: {
    height: 5,
    position: "absolute",
    top: 0,
    left: 10,
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
    borderRadius: 0,
    borderBottomStartRadius: 10,
    borderBottomRightRadius: 10,
  },
});

const mapStateToProps = (state) => {
  return {
    loginResult: state.authReducer.loginResult,
    isLogout: state.authReducer.isLogout,
  };
};

export default connect(mapStateToProps)(MainRoute);
