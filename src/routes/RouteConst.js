

export const SPLASH_PAGE = 'SplashPage';
export const LOGIN_PAGE = 'LoginPage';
export const FORGOT_PASSWORD_PAGE = 'ForgotPage'
export const HOME_PAGE = 'HomePage';
export const MORE_PAGE = 'MorePage';
export const REGISTER_PAGE = 'RegisterPage'
export const LOGIN_TAN_PAGE = 'LoginWithTan'
export const QUESTIONNAIRE_PAGE = 'QuestionnairePage'
export const APP_PAGE = 'AppStack';
