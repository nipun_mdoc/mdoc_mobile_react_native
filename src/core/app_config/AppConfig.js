import { mapFeaturesJSONToModel } from "../mapper/api_mapper/FeaturesListMapper";
import Config from 'react-native-config';

export const API_BASE_URL =  Config.API_URL;
export const ClientSecretKey =  Config.ClientSecretKey;
export const ClientID =  Config.ClientID;
const Feature = require('./Feature.json')

class AppConfig {

  /**
   * Constructor
   * */
  constructor(props) {
    this.envConfig = API_BASE_URL
  }


  /**
   * Is Debug
   * */
  static isDebug() {
    return __DEV__;
  }

  /**
   * WebServices API Configs.
   * */
  getAPIBaseUrl() {
    return API_BASE_URL;
  }

  /**
   * Client Secret Key.
   * */
   getClientSecretKey() {
    return ClientSecretKey;
  }

  /**
   * Client Id.
   * */
  getClientID() {
    return ClientID;
  }

  /*
  * Feature List
  * */

  getFeaturesList(){
    return mapFeaturesJSONToModel(Feature)
  }
}


export default AppConfig;
