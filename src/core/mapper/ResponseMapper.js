export function mapSuccessResponseJSONToModel(respJSON) {

  return {
    data: respJSON.data,
    code: respJSON.code, //false
    message: respJSON.message

  }
}

/* Sample
{
    "status": "1",   //0
    "error": false, /// true
    "data": null   ///message failure case
}
*/


export function mapFailureResponseJSONToModel(respJSON) {
  return {
    code: respJSON.code,
    message: respJSON.message
  }
}
