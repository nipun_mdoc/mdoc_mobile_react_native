export function mapFeaturesJSONToModel(respJSON) {
  return {
    AppColor: mapAppColorToModel(respJSON.appcolor),
    AppConfig: respJSON.appconfig && respJSON.appconfig.length > 0 ? mapAppConfigJSONArrayToModel(respJSON.appconfig) : []
  }
}

export function mapAppColorToModel(colorJson) {
  return {
    ColorPrimary: colorJson.colorPrimary,
    ColorAccent: colorJson.colorAccent,
    ColorTertiary: colorJson.colorTertiary
  }
}

export function mapAppConfigJSONArrayToModel(jsonArr) {
  let arr = [];
  jsonArr.forEach((el) => {
    let model = mapAppConfigDictToModel(el);
    arr.push(model);
  });

  return arr;
}

export function mapAppConfigDictToModel(respJSON) {
  return {
    Id: respJSON.id,
    IsEnabled: respJSON.isEnabled,
    Order: respJSON.order,
    FeatureTitle: respJSON.featureTitle,
    IconName: respJSON.iconName,
    Icon: respJSON.icon,
    SubConfig: respJSON.subConfig && respJSON.subConfig.length > 0 ? mapSubConfigJSONArrayToModel(respJSON.subConfig) : []
  }
}

export function mapSubConfigJSONArrayToModel(jsonArr) {
  let arr = [];
  jsonArr.forEach((el) => {
    let model = mapSubConfigDictToModel(el);
    arr.push(model);
  });

  return arr;
}
export function mapSubConfigDictToModel(respJSON) {
  return {
    Id: respJSON.id,
    IsEnabled: respJSON.isEnabled
  }
}
