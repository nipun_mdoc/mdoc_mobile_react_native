export function mapLoginJSONToModel(respJSON) {
  return {
    AccessToken: respJSON.accessToken,
    ExpireIn: respJSON.expiresIn,
    RefreshExpireIn: respJSON.refreshExpiresIn,
    RefreshToken: respJSON.refreshToken,
    TokenType: respJSON.tokenType,
    Scope: respJSON.scope
  }
}
