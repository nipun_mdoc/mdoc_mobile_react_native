import { LoginActionTypes } from "../actionTypes/AuthActionTypes";
import LoginService from "../../infra/service/LoginService";
import AppConfig from "../../core/app_config/AppConfig";
import { mapLoginJSONToModel } from "../../core/mapper/api_mapper/LoginMapper";

export const validateUserRequest = () => {
  return {
    type: LoginActionTypes.VALIDATE_USER_REQUEST
  }
};


export const validateUserSuccess = (authData) => {
  return {
    type: LoginActionTypes.VALIDATE_USER_SUCCESS,
    payload: authData
  }
};


export const validateUserFailure = (error) => {
  return {
    type: LoginActionTypes.VALIDATE_USER_FAILURE,
    payload: error
  }
};


export const validateUser = (deviceInfo, userName, password) => {
  let param = {
    client_secret: (new AppConfig()).getClientSecretKey(),
    password: password,
    client_id: (new AppConfig()).getClientID(),
    totp: "",
    friendlyName: deviceInfo,
    username: userName
  };
  console.log("login param:::::::", param)
  return async dispatch => {
    dispatch(validateUserRequest());
    try {
      let loginService = new LoginService();
      let loginData = await loginService.loginUser(param)
      if ( loginData.code === '200'){
        let userDara = mapLoginJSONToModel(loginData.data)
        // alert(JSON.stringify(userDara))
        // console.log("login data", JSON.stringify(userDara))
        dispatch(validateUserSuccess(userDara));
      }else {
        let error = Error(loginData.message)
        dispatch(validateUserFailure(error));
      }
    }
    catch(err) {
      console.log("error", err);
      dispatch(validateUserFailure(err));
    }
  }
};
