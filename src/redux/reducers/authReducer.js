import { LoginActionTypes } from "../actionTypes/AuthActionTypes";

const initialState = {
  inProgress: false,
  loginResult: null,
  error: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LoginActionTypes.VALIDATE_USER_REQUEST:
      return { ...state, inProgress: true };

    case LoginActionTypes.VALIDATE_USER_SUCCESS:
      return  { ...state, inProgress: false, loginResult: action.payload, error: null  };

    case LoginActionTypes.VALIDATE_USER_FAILURE:
      return { ...state, inProgress: false, error: action.payload };

    default:
      return state;
  }
};


export default authReducer;

