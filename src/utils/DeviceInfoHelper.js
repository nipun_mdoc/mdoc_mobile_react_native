import { getDeviceName, getUniqueId } from "react-native-device-info";

export default class DeviceDetail {
  static getUserDeviceWithUniqueId = async () => {
    let uniqueId = getUniqueId()
    let deviceName = await getDeviceName()
    return deviceName + " # " + uniqueId
  }
}

