import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { AppTheme } from "../app_style/AppTheme";

const AppButton = props => {
  const { onAction, title } = props;
  return (
    <View style={styles.viewBG}>
      <TouchableOpacity
        style={styles.subView}
        onPress={() => {
          onAction();
        }}
      >
        <Text
          style={styles.titleStyle}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  viewBG: {
    height: 48,
    width: '100%',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: AppTheme.COLOR.BLACK_COLOR
  },
  titleStyle: {
    fontSize: 16,
    color: AppTheme.COLOR.BLACK_COLOR,
    alignSelf: 'center',
  },
  subView: {
    height: '100%',
    width: '100%',
    justifyContent: 'center'
  }
});

export default AppButton;
