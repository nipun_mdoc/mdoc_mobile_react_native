import React from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import { AppTheme } from "../app_style/AppTheme";

const AppButtonBG = props => {
  const { onAction, title } = props;
  return (
    <View style={styles.viewBG}>
      <TouchableOpacity
        style={styles.subView}
        onPress={() => {
          onAction();
        }}
      >
        <Text
          style={styles.titleStyle}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  viewBG: {
    height: 48,
    width: '100%',
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
    borderRadius: 6
  },
  titleStyle: {
    fontSize: 16,
    color: AppTheme.COLOR.BLACK_COLOR,
    alignSelf: 'center',
  },
  subView: {
    height: '100%',
    width: '100%',
    justifyContent: 'center'
  }
});

export default AppButtonBG;
