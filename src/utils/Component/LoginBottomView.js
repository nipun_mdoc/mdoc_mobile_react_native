import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP } from "react-native-responsive-screen";
import { RFValue } from "react-native-responsive-fontsize";
import { STANDARD_SCREEN_HEIGHT } from "../Constant";

const LoginBottomView = props => {
  return (
    <View
      style={styles.mainViewStyle}>
      <TouchableOpacity
        style={styles.leftView}>
        <Text>Imprint</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.centerView}>
        <Text>Data Protection</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.rightView}>
        <Text>Conditions</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  mainViewStyle: {
    width: widthPercentageToDP("100%"),
    flexDirection: "row",
    marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT),
  },
  leftView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    borderRightWidth: 1,
  },
  centerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    borderRightWidth: 1,
  },
  rightView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
});

export default LoginBottomView;
