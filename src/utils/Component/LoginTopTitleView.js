import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Common } from "../../assets";
import { RFValue } from "react-native-responsive-fontsize";
import { STANDARD_SCREEN_HEIGHT } from "../Constant";
import { AppTheme } from "../app_style/AppTheme";

const LoginTopTitleView = props => {
  let {title} = props;
  return (
    <View
      style={styles.imageBGView}>
      <Image
        source={Common.SPLASH_LOGIN_IMG}
        style={styles.topImage}
        resizeMode={"contain"}
      />
      <Text
        style={styles.loginLabel}>
        {title}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  imageBGView: {
    width: "100%",
    height: RFValue(120, STANDARD_SCREEN_HEIGHT),
    justifyContent: "center",
    alignItems: "center",
  },
  topImage: {
    width: RFValue(160, STANDARD_SCREEN_HEIGHT),
    height: RFValue(80, STANDARD_SCREEN_HEIGHT),
    resizeMode: "contain",
  },
  loginLabel: {
    fontSize: RFValue(AppTheme.FONT_SIZE.LARGE_FONT_SIZE, STANDARD_SCREEN_HEIGHT),
    fontFamily: AppTheme.FONT_FAMILY.FONT_MEDIUM,
    justifyContent: "center",
    marginTop: RFValue(10, STANDARD_SCREEN_HEIGHT),
  },
});

export default LoginTopTitleView;
