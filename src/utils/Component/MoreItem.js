import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { AppTheme } from "../app_style/AppTheme";
import MoreMenuIcon from "./MoreMenuIcon";

const MoreItem = props => {
  const { onAction, item } = props;
  return (
    <View style={styles.mainBGView}>
      <TouchableOpacity
        onPress={() => {
          onAction(item)
        }}
      >
        <View style={styles.bgView}>
          <MoreMenuIcon item={item}/>
          <View style={styles.textBGView}>
            <Text style={styles.titleStyle}>{item.FeatureTitle}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  mainBGView: {
    padding: 8,
  },
  imgViewStyle: {
    height: 30,
    width: 30,
    resizeMode: "contain",
  },
  bgView: {
    flexDirection: "row",
    width: "100%",
  },
  titleStyle: {
    fontSize: AppTheme.FONT_SIZE.MENU_SIZE,
    lineHeight: 22,
    fontFamily: AppTheme.FONT_FAMILY.FONT_LIGHT,
  },
  textBGView: {
    justifyContent: "center",
    borderBottomWidth: 1,
    borderColor: AppTheme.COLOR.BORDER_COLOR,
    width: "100%",
    marginLeft: 10,
  },
});

export default MoreItem;
