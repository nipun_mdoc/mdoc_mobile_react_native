import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { TextInput } from "react-native-paper";
import { AppTheme } from "../app_style/AppTheme";
import FieldIcons from "./FieldIcons";
import { FieldIconType } from "../Constant";

const AppTextInput = props => {
  const [isPasswordVisible, setisPasswordVisible] = useState(false);

  const {
    valueText,
    setFieldValue,
    label,
    mode = "outlined",
    secureTextEntry = false,
    type = FieldIconType.NAME,
  } = props;
  return (
    <View>
      <TextInput
        label={label}
        value={valueText}
        onChangeText={text => setFieldValue(text)}
        mode={mode}
        secureTextEntry={secureTextEntry ? isPasswordVisible : false}
        theme={{
          colors: { primary: AppTheme.COLOR.THEME_COLOR, underlineColor: "transparent" },
        }}
        style={styles.viewStyle}
        left={<TextInput.Icon name={() => <FieldIcons type={type} />} />}
        right={secureTextEntry ? (<TextInput.Icon name={() => <TouchableOpacity
          onPress={() => {
            setisPasswordVisible(!isPasswordVisible);
          }}
        >
          <FieldIcons type={!isPasswordVisible ? FieldIconType.SHOW_PASSWORD : FieldIconType.HIDE_PASSWORD} />
        </TouchableOpacity>} />) : null}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    height: 48,
    borderRadius: 4,
    fontSize: AppTheme.FONT_SIZE.SMALL_FONT_SIZE,
    backgroundColor: AppTheme.COLOR.WHITE_COLOR,
  }
});

export default AppTextInput;
