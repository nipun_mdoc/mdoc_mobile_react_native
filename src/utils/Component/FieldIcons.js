import { View } from "react-native";
import React from "react";
import { AppTheme } from "../app_style/AppTheme";
import { FieldIconType } from "../Constant";
import EmailIcon from "../../assets/images/common/login/IconEmail.svg"
import InfoIcon from "../../assets/images/common/login/info.svg";
import PasswordIcon from "../../assets/images/common/login/IconKey.svg";
import PhoneIcon from "../../assets/images/common/login/IconPhone.svg";
import TanIcon from "../../assets/images/common/login/IconTAN.svg";
import UserIcon from "../../assets/images/common/login/IconUser.svg";
import NameIcon from "../../assets/images/common/login/IconUsernameFirst-Last.svg";
import ShowPassword from "../../assets/images/common/login/IconViewON.svg";
import HidePassword from "../../assets/images/common/login/IconViewOFF.svg";

const FieldIcons = props => {
  const { type } = props;
  switch (type) {
    case FieldIconType.EMAIL :
      return (
        <View style={{paddingTop: 10}}>
          <EmailIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.PASSWORD :
      return (
        <View style={{paddingTop: 10}}>
          <PasswordIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.PHONE :
      return (
        <View style={{paddingTop: 10}}>
          <PhoneIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.TAN :
      return (
        <View style={{paddingTop: 10}}>
          <TanIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.USER :
      return (
        <View style={{paddingTop: 10}}>
          <UserIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.NAME :
      return (
        <View style={{paddingTop: 10}}>
          <NameIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.SHOW_PASSWORD :
      return (
        <View style={{paddingTop: 10}}>
          <ShowPassword width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    case FieldIconType.HIDE_PASSWORD :
      return (
        <View style={{paddingTop: 10}}>
          <HidePassword width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR}/>
        </View>
      );
    default:
      return (
        <View style={{ padding: 5 }}>
          <InfoIcon width={20} height={20} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
  }
};
export default FieldIcons;
