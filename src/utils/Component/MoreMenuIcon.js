import { View } from "react-native";
import React from "react";
import { AppTheme } from "../app_style/AppTheme";
import DashboardIcon from "../../assets/images/common/more/IconModuleOverview.svg"
import MyProfileIcon from "../../assets/images/common/more/IconModuleMyProfile.svg";
import ChecklistIcon from "../../assets/images/common/more/IconModuleChecklist.svg"
import DevicesIcon from "../../assets/images/common/more/IconModuleDevices.svg"
import DiaryIcon from "../../assets/images/common/more/IconModuleDiary.svg"
import EntertainmentIcon from "../../assets/images/common/more/IconModuleEntertainment.svg"
import FileIcon from "../../assets/images/common/more/IconModuleFiles.svg"
import MediaIcon from "../../assets/images/common/more/IconModuleMedia.svg"
import MedicationIcon from "../../assets/images/common/more/IconModuleMedication.svg"
import MessagingIcon from "../../assets/images/common/more/IconModuleMessaging.svg"
import MyStayIcon from "../../assets/images/common/more/IconModuleMyStay.svg"
import PollenIcon from "../../assets/images/common/more/IconModulePollen.svg"
import VitalsIcon from "../../assets/images/common/more/IconModuleVitals.svg"
import Covid19Icon from "../../assets/images/common/more/IconModuleCovid19.svg"
import TherapyIcon from "../../assets/images/common/more/IconModuleCalendar_2.svg"
import MealIcon from "../../assets/images/common/more/IconModuleMealPlan.svg"
import QuestionnaireIcon from "../../assets/images/common/more/IconModuleQuestionnaire.svg"
import MyClinicIcon from "../../assets/images/common/more/IconModuleKiosk.svg"
import HelpSupportIcon from "../../assets/images/common/more/IconModuleHelp&Support.svg"
import PatientJourneyIcon from "../../assets/images/common/more/IconModulePatientJourney.svg"
import FamilyAccountIcon from "../../assets/images/common/more/IconModuleFamilyAccounts.svg"
import NotificationIcon from "../../assets/images/common/more/IconModuleNotifications.svg"
import ExerciseIcon from "../../assets/images/common/more/IconModuleExercises.svg"
import BookingIcon from "../../assets/images/common/more/IconModuleAppointmentBooking.svg"
import GoalIcon from "../../assets/images/common/more/IconModuleMyGoals.svg"
import ExternalServiceIcon from "../../assets/images/common/more/IconModuleExternalLinks.svg"
import { Features } from "../Constant";

const MoreMenuIcon = props => {
  const { item } = props;
  switch (item.Id) {
    case Features.DASHBOARD :
      return (
        <View style={{ padding: 5 }}>
          <DashboardIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MYPROFILE :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_COVID19 :
      return (
        <View style={{ padding: 5 }}>
          <Covid19Icon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_THERAPY :
      return (
        <View style={{ padding: 5 }}>
          <TherapyIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MEALS :
      return (
        <View style={{ padding: 5 }}>
          <MealIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_AIR_POLLEN :
      return (
        <View style={{ padding: 5 }}>
          <PollenIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_QUESTIONNAIRES :
      return (
        <View style={{ padding: 5 }}>
          <QuestionnaireIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MY_CLINIC :
      return (
        <View style={{ padding: 5 }}>
          <MyClinicIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_CHECKLIST :
      return (
        <View style={{ padding: 5 }}>
          <ChecklistIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_VITALS :
      return (
        <View style={{ padding: 5 }}>
          <VitalsIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_HELP_SUPPORT :
      return (
        <View style={{ padding: 5 }}>
          <HelpSupportIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MEDIA :
      return (
        <View style={{ padding: 5 }}>
          <MediaIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_PATIENT_JOURNEY :
      return (
        <View style={{ padding: 5 }}>
          <PatientJourneyIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_FILES :
      return (
        <View style={{ padding: 5 }}>
          <FileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_DIARY :
      return (
        <View style={{ padding: 5 }}>
          <DiaryIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_FAMILY_ACCOUNT :
      return (
        <View style={{ padding: 5 }}>
          <FamilyAccountIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_CONSENT :
      return (
        <View style={{ padding: 5 }}>
          <MedicationIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_NOTIFICATION :
      return (
        <View style={{ padding: 5 }}>
          <NotificationIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_EXERCISE :
      return (
        <View style={{ padding: 5 }}>
          <ExerciseIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_BOOKING :
      return (
        <View style={{ padding: 5 }}>
          <BookingIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MENTAL_GOALS :
      return (
        <View style={{ padding: 5 }}>
          <GoalIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MEDICATION_PLAN :
      return (
        <View style={{ padding: 5 }}>
          <MedicationIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_DEVICES :
      return (
        <View style={{ padding: 5 }}>
          <DevicesIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_ENTERTAINMENT :
      return (
        <View style={{ padding: 5 }}>
          <EntertainmentIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MESSAGE :
      return (
        <View style={{ padding: 5 }}>
          <MessagingIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_EXTERNAL_SERVICE :
      return (
        <View style={{ padding: 5 }}>
          <ExternalServiceIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MY_STAY :
      return (
        <View style={{ padding: 5 }}>
          <MyStayIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_COVID_HASHTAG :
      return (
        <View style={{ padding: 5 }}>
          <Covid19Icon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_NEWS :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_PAIR :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.CONFIG_PUSH_NOTIFICATION :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.CONFIG_ROOT_DETECTON_ENABLED :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.CONFIG_PRIVACY_POLICY :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    case Features.MODULE_MORE :
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
    default:
      return (
        <View style={{ padding: 5 }}>
          <MyProfileIcon width={24} height={24} fill={AppTheme.COLOR.BLACK_COLOR} />
        </View>
      );
  }
};
export default MoreMenuIcon;
