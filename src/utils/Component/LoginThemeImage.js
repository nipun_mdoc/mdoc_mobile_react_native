import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Common } from "../../assets";
import { RFValue } from "react-native-responsive-fontsize";
import { STANDARD_SCREEN_HEIGHT } from "../Constant";
import { AppTheme } from "../app_style/AppTheme";

const LoginThemeImage = props => {
  return (
    <View
      style={{
        width: "100%",
        height: RFValue(260, STANDARD_SCREEN_HEIGHT),
        justifyContent: "center",
        alignItems: "center",
      }}>
      <Image
        source={Common.LOGIN_HEADER_IMG}
        style={{
          width: RFValue(260, STANDARD_SCREEN_HEIGHT),
          height: RFValue(260, STANDARD_SCREEN_HEIGHT),
          resizeMode: "contain",
        }}
        resizeMode={"contain"}
      />
    </View>
  );
};

const styles = StyleSheet.create({

});

export default LoginThemeImage;
