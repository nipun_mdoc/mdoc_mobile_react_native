import React from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import { AppTheme } from "../app_style/AppTheme";

const TextButton = props => {
  const { onAction, title } = props;
  return (
    <View style={styles.viewStyle}>
      <TouchableOpacity
        style={styles.touchableViewStyle}
        onPress={() => {
          onAction();
        }}
      >
        <Text style={styles.titleStyle}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 14,
    color: AppTheme.COLOR.BLACK_COLOR,
    alignSelf: "center",
  },
  viewStyle: {
    height: 40,
    backgroundColor: 'transparent'
  },
  touchableViewStyle: {
    height: "100%",
    justifyContent: "center",
  },
});

export default TextButton;
