

import React from "react";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from "react-native-vector-icons/AntDesign";
import { AppTheme } from "./app_style/AppTheme";


export const EmailIcon = (size = 20, color = AppTheme.COLOR.BLACK_COLOR) => (<Ionicons name={'chevron-back'} size={size} color={color}/>);
export const ArrowLeft = (size = 20, color = AppTheme.COLOR.BLACK_COLOR) => (<AntDesign name={'arrowleft'} size={size} color={color}/>);
