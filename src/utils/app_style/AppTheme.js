export const AppTheme = {
  COLOR: {
    WHITE_COLOR : 'white',
    BLACK_COLOR : 'black',
    THEME_COLOR : '#EDC157',
    BORDER_COLOR : '#CCCCCC',
    PLACE_HOLDER_COLOR : '#CCCCCC'

  },

  TEXT_COLOR: {
    TITLE_COLOR : '#091400',
    GRAY_COLOR : '#666666'
  },

  FONT_SIZE: {
    EXTRA_SMALL_FONT_SIZE_2 : 9,
    EXTRA_SMALL_FONT_SIZE_1 : 10,
    EXTRA_SMALL_FONT_SIZE : 12,
    SMALL_FONT_SIZE : 14,
    NORMAL_FONT_SIZE : 16,
    LARGE_FONT_SIZE : 18,
    EXTRA_LARGE_FONT_SIZE : 20,
    EXTRA_LARGE_FONT_SIZE_2 : 24,
    EXTRA_LARGE_FONT_SIZE_3 : 26,
    MENU_SIZE : 17,
  },
  FONT_FAMILY: {
    FONT_BOLD : 'Poppins-Bold',
    FONT_LIGHT : 'Poppins-Light',
    FONT_MEDIUM : 'Poppins-Medium',
    FONT_REGULAR : 'Poppins-Regular',
    FONT_SEMI_BOLD : 'Poppins-SemiBold',
  }
};
