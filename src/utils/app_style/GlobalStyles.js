import { Platform, StatusBar, StyleSheet } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { RFValue } from "react-native-responsive-fontsize";
import { AppTheme } from "./AppTheme";
import { STANDARD_SCREEN_HEIGHT } from "../Constant";

const BAR_HEIGHT = StatusBar.currentHeight;

export default StyleSheet.create({
  centerView: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomContainer: {
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
    shadowColor: 'rgba(0, 0, 0, 0.19)',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowRadius: 30,
    elevation: 5,
    shadowOpacity: 1,
    height: RFValue(60, STANDARD_SCREEN_HEIGHT),
  },

  shadowEffect: {
    shadowColor: AppTheme.COLOR.PLACE_HOLDER_COLOR,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 1.84,

    elevation: 5,
  },

  bottomTabText: {
    color: AppTheme.COLOR.WHITE_COLOR,
    fontSize: RFValue(
      AppTheme.FONT_SIZE.SMALL_FONT_SIZE,
      STANDARD_SCREEN_HEIGHT,
    ),
    //  fontFamily: Constants.FONT_REGULAR,
  },

  card: {
    backgroundColor: AppTheme.COLOR.WHITE_COLOR,
    alignSelf: 'center',
    zIndex: 100,
    borderRadius: 10,
  },

  tabStyle: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingVertical: 4,
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  listContainerStyle: {
    flex: 1,
    width: wp('90%'),
    flexDirection: 'row',
    marginHorizontal: wp('5%'),
    //height: LIST_ITEM_HEIGHT,
    backgroundColor: AppTheme.COLOR.WHITE_COLOR,
    marginTop: RFValue(5, STANDARD_SCREEN_HEIGHT),
    marginBottom: RFValue(5, STANDARD_SCREEN_HEIGHT),
    borderRadius: RFValue(
      Platform.OS === 'ios' ? 5 : 1,
      STANDARD_SCREEN_HEIGHT,
    ),
    borderColor: AppTheme.COLOR.BLACK_COLOR,
    borderWidth: 1,
    //overflow: 'hidden',
    shadowColor: AppTheme.COLOR.BLACK_COLOR,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.8,
    shadowRadius: 1.41,
    elevation: 2,
  },
  listTitle: {
    color: AppTheme.COLOR.WHITE_COLOR,
    marginBottom: 4,
    marginLeft: RFValue(10, STANDARD_SCREEN_HEIGHT),
    fontSize: RFValue(
      AppTheme.FONT_SIZE.NORMAL_FONT_SIZE,
      STANDARD_SCREEN_HEIGHT,
    ),
    //  fontFamily: Constants.FONT_REGULAR,
  },

  topSafeArea: {
    flex: 0,
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
  },
  bottomSafeArea: {
    flex: 1,
    backgroundColor: AppTheme.COLOR.THEME_COLOR,
  },
  statusBar: {
    height: BAR_HEIGHT,
  },
  tabIconStyle: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  tabBarViewSelected: {
    height: '100%',
    justifyContent: 'center',
    width: '100%',
    borderTopColor: AppTheme.COLOR.BLACK_COLOR,
  },
  tabBarView: {
    height: '100%',
    justifyContent: 'center',
    width: '100%',
    borderTopWidth: 0,
  },
});
