import {Platform, StatusBar} from 'react-native';
import DeviceInfo from 'react-native-device-info';

export const Constants = {
  BottomMenu: {
    Overview: 'Overview',
    Media: 'Media',
    Questionnaires: 'Questionnaires',
    More: 'More',
  },
  LoginFieldType: {
    Username: 'Username',
    Password: 'Password'
  }
};

export const STANDARD_SCREEN_HEIGHT = 740;

export const STATUSBAR_HEIGHT =
  Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
export const APPBAR_HEIGHT = Platform.OS === 'ios' ? 30 : 56;
export const HEADER_BAR_HEIGHT = DeviceInfo.hasNotch()
  ? STATUSBAR_HEIGHT + APPBAR_HEIGHT
  : 0;


export const isObject = value => {
  return value !== null && typeof value === 'object';
};

export const Features = {
  DASHBOARD: 'DASHBOARD',
  MYPROFILE: 'MYPROFILE',
  MODULE_COVID19: 'MODULE_COVID19',
  MODULE_THERAPY: 'MODULE_THERAPY',
  MODULE_MEALS: 'MODULE_MEALS',
  MODULE_AIR_POLLEN: 'MODULE_AIR_POLLEN',
  MODULE_QUESTIONNAIRES: 'MODULE_QUESTIONNAIRES',
  MODULE_MY_CLINIC: 'MODULE_MY_CLINIC',
  MODULE_CHECKLIST: 'MODULE_CHECKLIST',
  MODULE_VITALS: 'MODULE_VITALS',
  MODULE_HELP_SUPPORT: 'MODULE_HELP_SUPPORT',
  MODULE_MEDIA: 'MODULE_MEDIA',
  MODULE_PATIENT_JOURNEY: 'MODULE_PATIENT_JOURNEY',
  MODULE_FILES: 'MODULE_FILES',
  MODULE_DIARY: 'MODULE_DIARY',
  MODULE_FAMILY_ACCOUNT: 'MODULE_FAMILY_ACCOUNT',
  MODULE_CONSENT: 'MODULE_CONSENT',
  MODULE_NOTIFICATION: 'MODULE_NOTIFICATION',
  MODULE_EXERCISE: 'MODULE_EXERCISE',
  MODULE_BOOKING: 'MODULE_BOOKING',
  MODULE_MENTAL_GOALS: 'MODULE_MENTAL_GOALS',
  MODULE_MEDICATION_PLAN: 'MODULE_MEDICATION_PLAN',
  MODULE_DEVICES: 'MODULE_DEVICES',
  MODULE_ENTERTAINMENT: 'MODULE_ENTERTAINMENT',
  MODULE_MESSAGE: 'MODULE_MESSAGE',
  MODULE_EXTERNAL_SERVICE: 'MODULE_EXTERNAL_SERVICE',
  MODULE_MY_STAY: 'MODULE_MY_STAY',
  MODULE_COVID_HASHTAG: 'MODULE_COVID_HASHTAG',
  MODULE_NEWS: 'MODULE_NEWS',
  MODULE_PAIR: 'MODULE_PAIR',
  CONFIG_PUSH_NOTIFICATION: 'CONFIG_PUSH_NOTIFICATION',
  CONFIG_ROOT_DETECTON_ENABLED: 'CONFIG_ROOT_DETECTON_ENABLED',
  CONFIG_PRIVACY_POLICY: 'CONFIG_PRIVACY_POLICY',
  MODULE_MORE: 'MODULE_MORE'
};

export const FieldIconType = {
  EMAIL: 'Email',
  PASSWORD: 'Password',
  PHONE: 'Phone',
  TAN: 'TAN',
  USER: 'USER',
  NAME: 'Name',
  SHOW_PASSWORD: 'ShowPassword',
  HIDE_PASSWORD: 'HidePassword'
};


